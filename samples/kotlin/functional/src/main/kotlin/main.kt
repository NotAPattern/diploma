fun main(args: Array<String>) {
    printInLambda()
}

fun printInLambda() {
    val days = listOf("Mon", "Tue", "Wen", "Thu", "Fri", "Sat", "Sun")
    val printLambda = { i: Int, day: String -> println("day[$i] = $day") }
    days.forEachIndexed(printLambda)
}