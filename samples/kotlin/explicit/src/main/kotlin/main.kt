import kotlin.text.StringBuilder

fun main(args: Array<String>) {
    print(listToString())
}

fun listToString(): String {
    val list = mutableListOf<Int>(1,2,3,4,5,6,7,8,9)

    val iter = list.listIterator()

    var result = StringBuilder()

    while(iter.hasNext()) {
        val number = iter.next()
        result.append("$number ")
    }

    return result.toString()
}