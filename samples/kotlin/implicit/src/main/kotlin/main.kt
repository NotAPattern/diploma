import kotlin.text.StringBuilder

fun main(args: Array<String>) {
    print(sample1())
}

fun sample1() : String {
    val days = listOf("Mon", "Tue", "Wen", "Thu", "Fri", "Sat", "Sun")
    val result = StringBuilder()
    for(day in days)
        result.append("$day ")
    return result.toString()
}