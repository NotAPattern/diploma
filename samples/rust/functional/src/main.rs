fn main() {
    let mut vector = vec![0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

    vector = vector.iter().map(|&x| x * x).collect();

    println!("{:?}", vector);
}
