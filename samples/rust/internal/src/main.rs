fn main() {
    let unmutable_array = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
    let mut mutable_array = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

    println!("Direct order:");
    direct_order_print_in_terminal(&unmutable_array);
    direct_order_print_in_terminal(&mutable_array);

    println!("Reverse order:");
    reverse_order_print_in_terminal(&unmutable_array);
    reverse_order_print_in_terminal(&mutable_array);

    println!("Another direct order:");
    mutable_array = [10, 11, 12, 13, 14, 15, 16, 17, 18, 19];
    another_direct_order_print(&mutable_array);
}

fn direct_order_print_in_terminal(array: &[u32]) {
    for iter in array {
        print!("{} ", iter);
    }
    print!("\n");
}

fn reverse_order_print_in_terminal(array: &[u32]) {
    for item in array.iter().rev() {
        print!("{} ", item);
    }
    print!("\n");
}

fn another_direct_order_print(array: &[u32]) {
    let mut iter = array.iter();
    while let Some(item) = iter.next() {
        print!("{} ", item);
    }
    println!("");
}
