use std::fmt;

pub struct Employee {
    name: String,
    salary: f64,
}

pub struct Employes {
    empl: Vec<Employee>,
    index: usize,
}

/*impl IntoIterator for Employes {
    type Item = f64;
    type IntoIter = Employes;

    fn into_iter(self) -> Self::IntoIter {
        Employes {
            empl: self,
            index: 0,
        }
    }
}*/

/*impl Employes {
    fn push(self, em: Employee) {
        self.empl.push(em);
    }
}*/

impl Iterator for Employes {
    type Item = f64;

    fn next(&mut self) -> Option<f64> {
        let result = self.empl[self.index].salary;
        self.index += 1;
        return Some(result);
    }
}

fn main() {
    // Автор ни в коем случае не желал создать в этом моменте гендерное неравенство, просто Алиса
    // более квалифицированный специалист.
    let person1 = Employee {
        name: "Alice".to_string(),
        salary: 200.0,
    };
    let person2 = Employee {
        name: "Bob".to_string(),
        salary: 100.0,
    };

    let e: Vec<Employee> = vec![person1, person2];

    let mut empls = Employes { empl: e, index: 0 };

    let mut sum: f64 = empls.next().unwrap();
    sum += empls.next().unwrap();
    sum /= 2 as f64;
    print!("{}", sum);
}
