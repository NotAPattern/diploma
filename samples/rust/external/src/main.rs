fn main() {
    let unmutable_vec: Vec<u32> = vec![0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
    let mut mutable_vec: Vec<u32> = vec![0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

    direct_order_print_in_terminal(&unmutable_vec);
    direct_order_print_in_terminal(&mutable_vec);

    reverse_order_print_in_terminal(&unmutable_vec);
    reverse_order_print_in_terminal(&mutable_vec);
}

fn direct_order_print_in_terminal(vector: &[u32]) {
    let mut iter = vector.iter();
    loop {
        match iter.next() {
            Some(value) => print!("{} ", value),
            None => break,
        }
    }
    print!("\n");
}

fn reverse_order_print_in_terminal(vector: &[u32]) {
    let mut iter = vector.iter().rev();
    loop {
        match iter.next() {
            Some(value) => print!("{} ", value),
            None => break,
        }
    }
    print!("\n");
}

// (0..10).for_each(|x| print!("{}", x));
