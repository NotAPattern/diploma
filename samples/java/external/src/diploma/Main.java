package diploma;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

public class Main {

    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<Integer>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);

        listOrderPrint(list);
        listReversePrint(list);
        listRemoveFive(list);
        listOrderPrint(list);
    }

    public static <T> void listOrderPrint(ArrayList<T> list) {
        Iterator<T> iter = list.listIterator();
        while (iter.hasNext()) {
            System.out.print(iter.next());
        }
        System.out.println();
    }

    public static <T> void listReversePrint(ArrayList<T> list) {
        ListIterator<T> iter = list.listIterator(list.size());
        while(iter.hasPrevious()) {
            System.out.print(iter.previous());
        }
        System.out.println();
    }

    public static <T> void listRemoveFive(ArrayList<T> list) {
        Iterator iter = list.iterator();
        while(iter.hasNext()) {
            Integer number = (Integer) iter.next();
            if(number == 5) {
                iter.remove();
            }
        }
    }

}
