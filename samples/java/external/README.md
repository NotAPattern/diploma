# external
В примере показано внешнее (явное) использование итераторов для ArrayList. На входе может быть ArrayList с любым типом данных.

# Запуск проекта
Для запуска проекта необходимо установить `idea`.

`Ubuntu`: `sudo apt install snap java-common && sudo snap install intellij-idea-community --classic --edge` 

# Сборка и запуск:
Откройте проект при помощи `IntelliJ IDEA`. Посе прогрузки нажмите зеленую кнопку play в левой верхней части окна. 
