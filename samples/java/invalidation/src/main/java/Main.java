import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<Integer>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(7);
        list.add(8);
        list.add(9);

        invalidationListRemoveFive(list);
    }

    // Инвалидация неявного итератора
    public static <T> void invalidationListRemoveFive(ArrayList<T> list) {
        for (T item : list) {
            if((Integer) item == 5) {
                try {
                    list.remove(item);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

public static <T> void validationListRemoveFive(ArrayList<T> list) {
    Iterator<T> iter = list.iterator();
    while (iter.hasNext()) {
        T number = iter.next();
        if (number.equals(5)) {
            number.remove();
        }
    }
}

}
