﻿using System;
using System.Collections.Generic;

namespace internal_iter
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Sample1();
        }

        public static void Sample1()
        {
            List<int> list = new List<int> { 0, 1, 2, 3, 4, 5 };
            foreach (int item in list)
            {
                Console.Write(item + " ");
            }
        }
}
}

