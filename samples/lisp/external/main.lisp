;https://rextester.com/l/common_lisp_online_compiler
;gnu clisp  2.49.60

(setq li (list 1 2 3 4 5))

(defvar *var* :value)
(setf *var* 0)

(defun iterate (&lis op) (loop for &x in &lis do (funcall op &x)))

(defun fu (op x) (funcall op x))

(defun sum (x) 
    (setf *var* (+ *var* x))
)

(iterate li 'sum)

(print *var*)
