#include <iostream>
#include <iterator>
#include <list>
#include <vector>

// Объявление функций.
template <class Type>
void invalidation(std::vector<Type> &vec);

template<class Type>
void validation(std::vector<Type> &vec);

int main() {
  // Инициалзиция вектора.
  std::vector<int> v{1, 2, 3, 4, 5};

  invalidation(v);

  v = {1,2,3,4,5};

  validation(v);
  // Так как в векторе нет функции push_front() (а итератор требудет наличие
  // этой функции), придется создать list.
  //  validation_front(l);

  return 0;
}

template <class Type>
void invalidation(std::vector<Type> &vec) {
  std::cout << "Invalidation:" << std::endl;
  // Обход всех элементов. Когда значение = 5, добавляем новую переменную.
  // Из-за этого область памяти ветора сдвигается, а облась памяти, на которую
  // указывает итератор, остается преждней. В зависимости от аппаратных средств
  // можно получить либо ошибку "segmentation fault", либо добавить еще один раз
  // цифру 6. В любом случае, итератор станет невалидным.
  for (auto it = vec.begin(); it != vec.end(); it++)
    if (*it == 5) vec.push_back(6);
  // Убедиться в этом можно при помощи вывода всех элементов вектора.
  for (auto it = vec.begin(); it != vec.end(); it++) std::cout << *it << " ";
  std::cout << std::endl;
}

// Пример будет работать корректно, если вставлять элементы после цифры 3.
template<class Type>
void validation_back(std::vector<Type> &vec) {
	// Переменная distance отвечает индекс в векторе.
  int distance;
  std::cout << "Correct work" << std::endl;
  auto it = vec.begin();
  while (it != vec.end()) {
    if (*it == 3) {
		// Получаем индекс переменной.
      distance = std::distance(vec.begin(), it);
	  // Записываем в конец новый элемент.
      vec.push_back(6);
	  // Делаем валидным старый итератор.
      it = vec.begin() + distance;
    }
    it++;
  }
  // Убедиться в этом можно при помощи вывода всех элементов вектора.
  for (auto it = vec.begin(); it != vec.end(); it++) std::cout << *it << " ";
  std::cout << std::endl;
}

