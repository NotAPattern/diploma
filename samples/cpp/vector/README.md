# vector
В примере реализауется класс DirectListIterator для контейнера vector из стандартной библиотеки std. 

# Установка:
Для запуска проекта необходимо установить `make`, `g++ >= 8.0`, `cmake`, `build-essential`, `checkinstall`.

`Ubuntu`: `sudo apt install make g++ cmake build-essential checkinstall` 

# Компиляция:
В каталоге проекта выполните команду:

`cmake . && make`

# Запуск:
Запуск проекта осуществляется при помощи команды:

`./vector`

