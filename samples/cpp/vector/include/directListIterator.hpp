#include <iterator.hpp>
#include <vector>

template <class Item>
class DirectVectorIterator : Iterator<Item> {
 public:
	 // Конструктор.
  DirectVectorIterator(const std::vector<Item>* aVector) {
    _vector = aVector;
    _current = 0;
  }
// Получение первого элемента.
  virtual void first() { _current = 0; }
// Переход к следующему.
  virtual void next() { _current = _current + 1; }
// Проверка на последний элемент.
  virtual bool isDone() const { return (_current == size(*_vector)); }
// Функция получения текущего элемента.
  virtual Item currentItem() const { return _vector->at(_current); }

 private:
  // Указатель на структура данных, для которой реализуем итератор.
  const std::vector<Item>* _vector;
  // Переменнная-счетчик
  long _current;
};
