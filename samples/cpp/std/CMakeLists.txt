# Минимальная версия Cmake
cmake_minimum_required(VERSION 3.17)
# Название проекта
project(std)
# Выбор стандарта C++
set(CMAKE_CXX_STANDARD 17)
# Назначить файлы в проект
add_executable(std main.cpp)
