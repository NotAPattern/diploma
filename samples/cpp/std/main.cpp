#include <iostream>
#include <list>
#include <vector>

// Объявления шаблонных функций для вывода на экран.
template <class Data>
void directOrderPrintInTerminal(Data data);

template <typename Data>
void reverseOrderPrintInTerminal(Data data);

int main() {
  // Инициализация векторов
  std::vector<int> vector = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
  std::vector<char> char_vector = {'S', 't', 'r', 'i', 'n', 'g'};

  std::cout << "For vectors:" << std::endl;
  // Вызов функций для контейнеров (структур данных) vector,
  // но разных типов данных для печати на экране в прямом порядке.
  directOrderPrintInTerminal(vector);
  directOrderPrintInTerminal(char_vector);
  // Вызов функций для контейнеров (структур данных) vector,
  // но разных типов данных для печати на экране в обратном порядке.
  reverseOrderPrintInTerminal(vector);
  reverseOrderPrintInTerminal(char_vector);

  std::list<uint16_t> int_list = {0, 1, 2, 3, 4, 5};
  std::list<char> char_list = {'A', 'n', 'o', 't', 'h', 'e', 'r',
                               ' ', 'S', 't', 'r', 'i', 'n', 'g'};

  std::cout << "For lists:" << std::endl;
  // Вызов функций для контейнеров (структур данных) list,
  // но разных типов данных для печати на экране в прямом порядке.
  directOrderPrintInTerminal(int_list);
  directOrderPrintInTerminal(char_list);
  // Вызов функций для контейнеров (структур данных) list,
  // но разных типов данных для печати на экране в обратном порядке.
  reverseOrderPrintInTerminal(int_list);
  reverseOrderPrintInTerminal(char_list);

  return 0;
}

// Определение шабонных функций, объявленных выше.
template <typename Data>
void directOrderPrintInTerminal(Data data) {
  // Получаем первый элемент контейнера (структуры данных).
  auto iter = data.begin();
  // Проходим последовательно по циклу, пока итератор
  // не будет указывать на конец последовательности.
  for (iter; iter != data.end(); iter++) {
    // Получаем элемент при помощи оператора "*".
    std::cout << *iter;
  }
  // Перевод на новую строку.
  std::cout << std::endl;
}

template <typename Data>
void reverseOrderPrintInTerminal(Data data) {
  auto iter = data.rbegin();
  // Проходим последовательно по циклу в обратном направлении,
  // пока итератор не будет указывать на конец последовательности.
  for (iter; iter != data.rend(); iter++) {
    // Получаем элемент при помощи оператора "*".
    std::cout << *iter;
  }
  // Перевод на новую строку.
  std::cout << std::endl;
}
