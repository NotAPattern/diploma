#include <array>

// Описание абстрактного класса для
// составления сигнатур.
// Методы не виртуальные для упрощения
// иерархии классов.
template <typename Item>
class AbstactComposite {
 public:
  // Операция добавить элемент.
  void add(Item item);
  // Операция удалить элемент.
  void remove(Item item);
  // Получение элемента
  Item get();
};

template <typename Item>
class Composite: AbstactComposite<Item> {
 protected:
  std::array<Item, n> _children;
 public:
  
};

// Объявим класс бинарных деревьев.
// У них существует только левый и правый потомок.
// Реализация в файле tree.cpp в src/
template <typename Item>
class BinaryComposite : protected AbstractComposite<Item> {
 protected:
  // Левый потомок
  BinaryTree* _leftChildren;
  // Правый потомок
  BinaryTree* _rightChildren;

 public:
  void add(Item item) {}
};
